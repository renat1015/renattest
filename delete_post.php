<?php 
	//var_dump($_POST);
	require_once("connect.php");
	try {
		if (!empty($_POST)) {
			$error = [];
			if (empty($_POST['id'])) {
			$error[] = "Отсутствует id";
			}
			if (empty($error)) {
				$query = "SELECT * FROM posts WHERE id = ?";
				$com = $pdo->prepare($query);
				$com->execute([$_POST['id']]);
				$file_name = $com->fetch()['file'];

				if ($file_name != "") {
					$way = "upload/";
					$file = $way . $file_name;
					if (file_exists($file)) {
						unlink($file);
					}
				}

				$query = "DELETE FROM posts WHERE id = :id";
				$usr = $pdo->prepare($query);
				$usr->execute(['id' => $_POST['id'],]);
			}
		}
		require_once("view_post.php");
	} catch (PDOException $e) {
		echo "Ошибка выполнения запроса: " . $e->getMessage();
	}
?>