<?php 
	//var_dump($_POST);
	require_once("connect.php");
	try {
		if (!empty($_POST)) {
			$error = [];
			if (empty($_POST['id'])) {
			$error[] = "Отсутствует id";
			}
			if (empty($_POST['content'])) {
			$error[] = "Отсутствует текст";
			}
			if (empty($error)) {
				$query = "UPDATE posts SET content = :content WHERE id = :id";
				$usr = $pdo->prepare($query);
				$usr->execute([
					'content' => $_POST['content'],
					'id' => $_POST['id'],
				]);
			}
		}
		require_once("view_post.php");
	} catch (PDOException $e) {
		echo "Ошибка выполнения запроса: " . $e->getMessage();
	}
?>