<?php 
	//var_dump($_POST);
	require_once("connect.php");
	try {
		if (!empty($_POST)) {
			$error = [];
			if (empty($_POST['Text'])) {
			$error[] = "Отсутствует текст";
			}
			if (empty($error)) {
				$query = "INSERT INTO posts VALUES (NULL, :content, :file)";
				$usr = $pdo->prepare($query);
				$usr->execute([
					'content' => $_POST['Text'],
					'file' => $_FILES['File']['name'],
				]);
				move_uploaded_file($_FILES['File']['tmp_name'], 'upload/' . $_FILES['File']['name']);
			}
		}
		require_once("view_post.php");
	} catch (PDOException $e) {
		echo "Ошибка выполнения запроса: " . $e->getMessage();
	}
?>