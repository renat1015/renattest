
<!DOCTYPE html>
<html>
<head>
	<title>Тестовое задание</title>
	<meta charset="utf-8">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$("#submit-id").on("click", function() {
				$("#submit-id").prop("disabled", true);
				var form_data = new FormData($('form')[0]);
				$.ajax({
					url: "create_post.php",
					method: 'post',
					data: form_data,
				    processData: false,
				    contentType: false,
				    success: function() {}
				}) .done(function(data) {
					$("#info").html(data);
					$("#submit-id").prop("disabled", false); 
				});
			})
		});
	</script>
	<script type="text/javascript">
		$(document).ready(function() {
			$(".del").on("click", function() {
				var form_data2 = new FormData();
				form_data2.append('id', this.id);
				alert("Должны удалить запись " + (this.id));
				$.ajax({
					url: "delete_post.php",
					method: 'post',
					data: form_data2,
				    processData: false,
				    contentType: false,
				    success: function() {}
				}) .done(function(data) {
					$("#info").html(data); 
				});
			})
		});
	</script>
	<script type="text/javascript">
		$(document).ready(function() {
			$(".upd").on("click", function() {
				let text = prompt('Введите новый текст', "...");
				var form_data3 = new FormData();
				form_data3.append('id', this.id);
				form_data3.append('content', text);
				$.ajax({
					url: "update_post.php",
					method: 'post',
					data: form_data3,
				    processData: false,
				    contentType: false,
				    success: function() {}
				}) .done(function(data) {
					$("#info").html(data); 
				});
			})
		});
	</script>
</head>
<body>
	<div id="form">
		<form enctype="multipart/form-data">
			<p>Введите текст:</p>
			<p><textarea id="content" name="Text" cols="50" rows="5"></textarea></p>
			<p><input id="file" type="file" name="File"></p>
			<p><input id="submit-id" type="submit" name="Save"></p>	
		</form>
	</div>
	<div id="info">
	<?php
		require_once("view_post.php");
	?>	
	</div>
</body>
</html>